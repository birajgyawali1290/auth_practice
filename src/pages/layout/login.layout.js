import { useState } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import "../../assets/css/global.css";
import { NavLink } from "react-router-dom";
import { auth_svc } from "../../service/auth.service";
import { toast } from "react-toastify";
const LoginLayout = () => {
  let [data, setData] = useState({
    email: null,
    password: null,
  });
  const handleChange = (e) => {
    setData({
      ...data,
      [e.target.name]: e.target.value,
    });
  };

  let handleSubmit = async (e) => {
    try {
      e.preventDefault();
      let result = await auth_svc.login(data);
      toast.success("Welcome to " + result.role + " panel");
    } catch (error) {
        if(error.response?.status === 400){
            if(error.response.data.msg){
                toast.warn(error.response.data.msg)
            }
        }else{
            toast.error(error.response.data.msg)
        }
    }
  };
  return (
    <div className="login-page">
      <Container>
        <Row className="justify-content-center align-items-center">
          <Col md={6}>
            <div className="login-form">
              <h2>Login</h2>
              <hr />
              <Form onSubmit={handleSubmit}>
                <Form.Group controlId="formBasicEmail">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    onChange={handleChange}
                    name="email"
                  />
                </Form.Group>

                <Form.Group controlId="formBasicPassword" className="my-3">
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    onChange={handleChange}
                    name="password"
                  />
                </Form.Group>

                <Button variant="success" type="submit">
                  Login
                </Button>
              </Form>
              <NavLink to={"/register"}>Register</NavLink>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default LoginLayout;
