import LoginLayout from "./login.layout"
import RegisterLayout from "./register.layout";
const LayoutPage = {
    LoginLayout,
    RegisterLayout
}
export default LayoutPage;