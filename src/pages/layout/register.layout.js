import { Form, Button, Container, Row, Col } from "react-bootstrap";
import "../../assets/css/global.css";
import { NavLink, useNavigate } from "react-router-dom";
import * as Yup from "yup";
import { useFormik } from "formik";
import { auth_svc } from "../../service/auth.service";
import { toast } from "react-toastify";

const RegisterLayout = () => {
  let navigate = useNavigate();
  const defaultValue = {
    name: "",
    email: "",
    password: "",
    role: "",
    status: "",
    mobile: "",
    address: "",
    image: "",
  };
  const validate = Yup.object({
    name: Yup.string().required("Name is required"),
    email: Yup.string().email().required("Email is required"),
    password: Yup.string().required("Password is required"),
    role: Yup.string().required(),
    status: Yup.string().required(),
    mobile: Yup.number().required(),
    address: Yup.string(),
    image: Yup.string(),
  });
  const formik = useFormik({
    initialValues: defaultValue,
    validationSchema: validate,
    onSubmit: (values) => {},
  });
  const imageFilter = (e) => {
    let { files } = e.target;
    let file = files[0];
    let allowed_image = ["jpg", "jpeg", "gif", "svg", "bmp", "webp", "png"];
    let split_image = file.name.split(".");
    let ext = split_image.pop();
    if (allowed_image.includes(ext.toLowerCase())) {
      if (file.size < 5 * 1024 * 1024) {
        formik.setValues({
          ...formik.values,
          image: file,
        });
      } else {
        formik.setErrors({
          ...formik.errors,
          image: "File should be less than 5 mb",
        });
      }
    } else {
      formik.setErrors({
        ...formik.errors,
        image: "Image type not supported",
      });
    }
  };
  const handleSubmit = async (e) => {
    try {
      e.preventDefault();
      formik.handleSubmit();
      let values = {
        name: formik.values.name,
        email: formik.values.email,
        password: formik.values.password,
        role: formik.values.role,
        status: formik.values.status,
        mobile: formik.values.mobile,
        address: formik.values.address,
        image: formik.values.image.name,
      };
      let result = await auth_svc.register(values);
      if (result.data) {
        toast.success(result.data.msg);
        navigate("/");
      }
    } catch (error) {
      console.log("Register Error:", error);
    }
  };
  return (
    <>
      <div className="login-page">
        <Container>
          <Row className="justify-content-center align-items-center">
            <Col md={6}>
              <div className="login-form">
                <h2>Register</h2>
                <hr />
                <Form onSubmit={handleSubmit}>
                  <Form.Group>
                    <Form.Label>Name</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter your name..."
                      onChange={formik.handleChange}
                      name="name"
                    />
                    <span className="text-danger">{formik.errors?.name}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control
                      type="email"
                      placeholder="Enter your email..."
                      onChange={formik.handleChange}
                      name="email"
                    />
                    <span className="text-danger">{formik.errors?.email}</span>
                  </Form.Group>

                  <Form.Group>
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="password"
                      placeholder="Enter your password..."
                      onChange={formik.handleChange}
                      name="password"
                    />
                    <span className="text-danger">
                      {formik.errors?.password}
                    </span>
                  </Form.Group>

                  <Form.Group className="my-3">
                    <Form.Label>Role</Form.Label>
                    <Form.Select
                      as="select"
                      name="role"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors.role : ""}>
                        --Select One Option--
                      </option>
                      <option value={"customer"}>Customer</option>
                      <option value={"seller"}>Seller</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors?.role}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Status</Form.Label>
                    <Form.Select
                      as="select"
                      name="status"
                      onChange={formik.handleChange}
                    >
                      <option value={"" ? formik.errors.status : ""}>
                        --Select One Option--
                      </option>
                      <option value={"active"}>Active</option>
                      <option value={"inactive"}>Inactive</option>
                    </Form.Select>
                    <span className="text-danger">{formik.errors?.status}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Mobile</Form.Label>
                    <Form.Control
                      type="number"
                      placeholder="Enter your mobile number..."
                      onChange={formik.handleChange}
                      name="mobile"
                    />
                    <span className="text-danger">{formik.errors?.mobile}</span>
                  </Form.Group>
                  <Form.Group className="my-3">
                    <Form.Label>Address</Form.Label>
                    <Form.Control
                      type="text"
                      placeholder="Enter your current address"
                      onChange={formik.handleChange}
                      name="address"
                    />
                  </Form.Group>
                  <Form.Group className="my-3 row">
                    <Form.Label>Image</Form.Label>
                    <Col sm={7}>
                      <Form.Control
                        type="file"
                        onChange={imageFilter}
                        name="image"
                      />
                    </Col>
                    <Col sm={4}>
                      {formik.values.image ? (
                        <>
                          <img
                            src={URL.createObjectURL(formik.values.image)}
                            className="img img-fluid"
                          />
                        </>
                      ) : (
                        ""
                      )}
                    </Col>
                    <span className="text-danger">{formik.errors?.image}</span>
                  </Form.Group>

                  <Button variant="success" type="submit">
                    Register
                  </Button>
                </Form>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    </>
  );
};
export default RegisterLayout;
