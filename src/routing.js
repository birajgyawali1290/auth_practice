import { BrowserRouter, Route, Routes } from "react-router-dom";
import LayoutPage from "./pages/layout";
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
const Routing = () => {
  return (
    <>
      <ToastContainer />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<LayoutPage.LoginLayout />} />
          <Route path="/register" element={<LayoutPage.RegisterLayout />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default Routing;
