import axiosInstance from "../config/axios.connect.config";

class HttpService{
    header = {}
    getHeader = (config) => {
        if(config.login){
            let token = localStorage.getItem("auth_token");
            this.header = {
                'authorization': "Bearer "+token,
                'content-type': "application/json"
            }
        }
        if(config.files){
            this.header = {
                ...this.header,
                "content-type": "multipart/form-data"
            }
        }
    }

    postRequest = async(url, data , config={}) => {
        try{
            this.getHeader(config);
            let response = await axiosInstance.post(url, data, {
                headers: this.header
            });
            return response
        }catch(error){
            throw error
        }
    }
}
export default HttpService;