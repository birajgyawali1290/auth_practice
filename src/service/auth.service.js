import HttpService from "./axios.service";

class AuthService extends HttpService {
  login = async (data) => {
    try {
      let response = await this.postRequest("login", data);
      let local_user = {
        name: response.data.result.user.name,
        email: response.data.result.user.email,
        role: response.data.result.user.role,
        user_id: response.data.result.user._id,
      };
      localStorage.setItem("auth_token", response.data.result.access_token);
      return local_user;
    } catch (error) {
      console.error("Login Error", error);
      throw error
    }
  };
  register = async(data) => {
    try{
        let response = await this.postRequest('register', data, {login: true})
        return response
    }catch(error){
        throw error
    }
  }
}
export const auth_svc = new AuthService();
export default AuthService;
