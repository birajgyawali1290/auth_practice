import axios from "axios";
import ErrResponse from "../helper/http-status-code";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_URL,
  timeout: 20000,
  timeoutErrorMessage: "Server time out!",
  headers: {
    "Content-Type": "application/json",
  },
});

axios.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    ErrResponse(error);
  }
);
export default axiosInstance;