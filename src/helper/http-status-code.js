import { StatusCodes } from "http-status-codes"
import { toast } from "react-toastify"

const ErrResponse = (error) => {
    if(error.response.status === StatusCodes.BAD_REQUEST){
        return toast.error(error.msg)
    }else if(error.response.status === StatusCodes.INTERNAL_SERVER_ERROR){
        return toast.error(error.msg)
    }else if(error.response.status === StatusCodes.NOT_FOUND){
        return toast.error(error.msg)
    }else if(error.response.status === StatusCodes.UNAUTHORIZED){
        localStorage.removeItem('auth_token')
        window.location.href("/")
        return toast.error(error.msg)
    }
}
export default ErrResponse;